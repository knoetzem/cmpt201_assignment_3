/*-----------------------------------------------------------------
# Martin Knoetze
# Assignment 3
# Lab Section: AS02(1), Winter 2021
# CMPUT 201, Winter 2021
# Lab Instructor's Name: Antal Buss
#
# Program Purpose:
# This is the source file for the blockChain module
#
# This module implements a block chain to store a peep. It allows
# for the storing and printing of a peep, a string of up to 256
# characters. The contents of a block chain can be writen to a 
# binary text file and also an already started block chain can
# be read from a binary text file.
*----------------------------------------------------------------*/

#include "blockChain.h"
/*------------------------------------------------------------------------------------------------------------
# Prescribed functions and related helper functions
*-----------------------------------------------------------------------------------------------------------*/

/* 
	unifyTransaction(tranasction transaction)
	takes the contents of a transaction and puts it into a character array 
	PARAMS:	transaction	- a transaction to be put into a character array 
  	RETURN:	a character array to represent a transaction
  	PRE:	a transaction has been appropriatly populated
  	POST:	the contents of a transaction has been put into a character array
*/
unsigned char * unifyTransaction(transaction transaction){
	unsigned char * transactionArray = malloc(sizeof(transaction));

	for (int j = 0; j < 6; j++)
        transactionArray[j] = transaction.timeStamp[j];
    transactionArray[6] = transaction.modification.position;
    transactionArray[7] = transaction.modification.character << 1;
    transactionArray[7] += transaction.modification.event_type;

	return transactionArray;
}

/* 
	writeBlockChain(char * fileName)
	writes a block chain to a file
	PARAMS:	fileName 	- a string with the name of the file to which the block chain gets writen
  	RETURN:	none
  	PRE:	a ledger of blocks and pointers has been created and populated
  	POST:	the contents of the ledger has been writen to a binary file
*/
void writeBlockChain(char * fileName){
	struct blockChain *ledgerPtr = ledger;
	FILE *fp;
	unsigned char * transaction;

	if(ledger == NULL || ledger->b.transactions == NULL){
		printf("Ledger is empty\n");
		return;
	}

	// create a new file or open a file with name fileName to be writen to
	fp = fopen(fileName, "w");

	do{
		// write the contents of the block minus the transactions to the file
	 	fwrite(&ledgerPtr->b.data, 1, sizeof(ledgerPtr->b.data), fp);
	 	// write the the transactions to the file
		for (int i = 0; i < ledgerPtr->b.data[17]; i++){
			transaction = unifyTransaction(ledgerPtr->b.transactions[i]);
			fwrite(transaction, 8, 1, fp);
		}
		// advance the ledger
	 	ledgerPtr = ledgerPtr->next;
	} while (ledgerPtr != NULL);
	fclose(fp);
}

/* 
	readBlockChain(char * fileName)
	reads a block chain from a file
	PARAMS: fileName 	- the name of the file from which to read a blockChain
  	RETURN: none
  	PRE:	the file specified by fileName exists
  	POST:	the block chain has been read from the text file and a blockChain linked list 
	  		has been populated with the data from the text file
*/
void readBlockChain(char * fileName){
	FILE * fp;
	struct blockChain * ledgerPtr;
	char transaction[8];
	char c;

	// if the ledger has contents then it is freed to be used to store the block chain to be
	// read form the binary file
	if(ledger != NULL)
		freeLedger();
	
	initializeLedger();

	ledgerPtr = ledger;
	
	// open file fileName
	fp = fopen(fileName, "r");
	// enures a file was actually opened
	if (fp == NULL){
        printf("No such file exists\n");
    }
	// read only the character to be able to check if the file is at it's end
	fread(&c, 1, 1, fp);
	// while loop reads the content of the file until it reaches the end of the file
	while(!feof(fp)){
		// checks if the ledger has yet to be populated
		// this is done in this fassion in order to not allocate another blockChain after all the
		// relevant data has been read from the file
		if (ledgerPtr->b.transactions != NULL){
			// if ledger has been populated then space is allocated for a new block chain element
			ledgerPtr->next = malloc(sizeof(struct blockChain));
			// lePtr is advaced to point to the new block chain element
			ledgerPtr = ledgerPtr->next;
		}
		// if reading the single character in the text file didn't put the fp at the end of the text file then it the 
		// character needs to be put into the data portion of the block 
		ledgerPtr->b.data[0] = c;
		// reads the remaining characters that are to be stored in the data portion of the current block in the block chain
		fread(ledgerPtr->b.data + 1, sizeof(ledgerPtr->b.data[0]), sizeof(ledgerPtr->b.data) - 1, fp);

		// assigns space for the transactions of the current block
		ledgerPtr->b.transactions = malloc(ledgerPtr->b.data[17] * sizeof(transaction));

		// reads the data for the transactions stored in the text file
		for (int i = 0; i < ledgerPtr->b.data[17]; i++)	{
			fread(&transaction, sizeof(transaction), 1, fp);
			for (int j = 0; j < 6; j++)
				ledgerPtr->b.transactions[i].timeStamp[j] = transaction[j];
			ledgerPtr->b.transactions[i].modification.position = transaction[6];
			ledgerPtr->b.transactions[i].modification.character = transaction[7] >> 1;
			ledgerPtr->b.transactions[i].modification.event_type = transaction[7] & 0x01;
		}
		// read only the character to be able to check if the file is at it's end
		fread(&c, 1, 1, fp);
	}
	// close the file
	fclose(fp);
}

/* 
	setBlockTimeStamp(struct blockChain * ledgerPtr)
	sets a block's time stamp
	PARAMS: ledgerPtr	- a pointer to an instance of a blockChain containing the block that is getting the timeStamp
  	RETURN:	none
  	PRE:	a blockChain instance holding a block has been created
  	POST:	the block in the blockChain instance has a time stamp assigned to it
*/
void setBlockTimeStamp(struct blockChain * ledgerPtr){
	struct tm * t;
	time_t timeStamp;

	// gets the epoch time since 1900
	timeStamp = time(NULL);

	// generates the tm values based on the epoch time just assinged to timeStamp
	t = gmtime(&timeStamp);

	// gets the number of years since 2020 to be able to store the time stamp in 6 bytes
	t->tm_year -= 2020 - 1900;

	// stores the time stamp in the block
	ledgerPtr->b.data[7] = (char)t->tm_year;
	ledgerPtr->b.data[8] = (char)t->tm_mon;
	ledgerPtr->b.data[9] = (char)t->tm_mday;
	ledgerPtr->b.data[10] = (char)t->tm_hour;
	ledgerPtr->b.data[11] = (char)t->tm_min;
	ledgerPtr->b.data[12] = (char)t->tm_sec;
}

/*
	getHash(unsigned char * hashable, int hashableLength)
	calculates and returns hash of a list if chars
	PARAMS: hashable 		- a pointer to a list of characters to be hashed
			hashableLength	- the length of the list of character
  	RETURN:	returns the sha256 hash value of the hashable
  	PRE:	a list of characters exists and the length has been calculated
  	POST:	the hash of the list of characters has been calculated and returned
*/
unsigned char * getHash(unsigned char * hashable, int hashableLength){
	// The variable digest_len will hold the length of the
	// sha256 digest
    unsigned int digest_len=EVP_MD_size(EVP_sha256());

	// The variable digest will hold the final digest (hash)
	unsigned char *digest = (unsigned char *)OPENSSL_malloc(EVP_MD_size(EVP_sha256()));

	// Declare the message digest context variable
    EVP_MD_CTX *mdctx;
    
    //create a message digest context
    mdctx = EVP_MD_CTX_create();
    
    // Initialise the context by identifying the algorithm to be used
    EVP_DigestInit_ex(mdctx, EVP_sha256(), NULL);

	// Update the digest with message
    EVP_DigestUpdate(mdctx, hashable, hashableLength);

	// finalize digest
    EVP_DigestFinal_ex(mdctx, digest, &digest_len);

	EVP_MD_CTX_destroy(mdctx);
	
	// ensures the pointer returned has a null character at the end of the 32 characters in the hash value
	digest[digest_len] = '\0';
	
	return digest;
}

/*
	getMekleRoot(struct transaction * transactions, int num)
	calculates the merkle root of a list of transactions
	PARAMS: transactions	- a pointer to a list of transactions
			num				- the number of transactions 
  	RETURN:	returns the merkle root of the transactions
  	PRE:	a list of transactions exists and the number of transactions has been calculated
  	POST:	the hash of the list of characters has been calculated and returned
*/
unsigned char * getMerkleRoot(transaction * transactions, int num){

	unsigned char * transactionPtr;
	unsigned char * hash;
	unsigned char hashList[num][32];
	unsigned char concatHash[64];
	int nodes = num;

	// hash all the transactions and store their resulting hashes in hashList
	for (int i = 0; i < num; i++){
		// assign transactionPtr to point to the list of chars that make up the transaction
		transactionPtr = unifyTransaction(transactions[i]);
		hash = getHash(transactionPtr, 8);
		// store the hash in hashList
		for (int j = 0; j < 32; j++)
			hashList[i][j] = hash[j];
		free(hash);
	}

	// num represents the number of nodes at a level of the tree starting with the total number of
	// transactions for leaf nodes. So, while there are more than 1 node at a level in the tree the
	// program continues working towards calculating the Merkle root 
	while (nodes > 1){
		// concatonates two hashes and calculates their hash
		for (int i = 0; i < nodes/2; i++){
			//concatHash = (unsigned char *)strncat((char*)(hashList[i * 2]), (char*)(hashList[(i * 2) + 1]), 32);
			for (int j = 0; j < 64; j++){
				if(j < 32)
					concatHash[j] = hashList[i * 2][j];
				else
					concatHash[j] = hashList[(i * 2) + 1][j-32];
			}
			hash = getHash(concatHash, 64);
			for (int j = 0; j < 32; j++)
				hashList[i][j] = hash[j];
			free(hash);
		}
		// if there are an uneven number of nodes in the level of the tree then concatonates the last hash with itself
		// and calculates the hash of the result
		if (nodes % 2 == 1){
			for (int j = 0; j < 64; j++){
				if(j < 32)
					concatHash[j] = hashList[nodes - 1][j];
				else
					concatHash[j] = hashList[nodes - 1][j-32];
			}
			hash = getHash(concatHash, 64);
			for (int j = 0; j < 32; j++)
				hashList[nodes/2][j] = hash[j];
			free(hash);
			nodes++;
		}
		// divide num by two get the number of nodes in the next level up in the tree
		nodes = nodes / 2;
	}

	hash = malloc(32);

	for (int i = 0; i < 32; i++){
		hash[i] = hashList[0][i];
	}
	// return the Merkle root
	return hash;
}

/* 
	verifyBlockChain()
	verifies the integrity of the block chain
	PARAMS:	none
  	RETURN:	returns 0 if valid and 1 if invalid
  	PRE:	the block chain ledger has been populated
  	POST:	the block chain has been parsed and verified
*/
int verifyBlockChain(){
	struct blockChain * ledgerPtr = ledger;
	unsigned char *hash;

	// verifies the ledger has actually been populated
	if(ledgerPtr->b.transactions == NULL){
		printf("Ledger is empty\n");
		return 1;
	}

	while (ledgerPtr != NULL){
		// if not the first block
		if ((int)(ledgerPtr->b.data[13]) != 1){
			// compare current block's head hash to the next block's previous hash
			if (ledgerPtr->next != NULL){
				for (int i = 26; i < 31; i++){
					if(ledgerPtr->b.data[i] != ledgerPtr->next->b.data[i-26])
						return 1;
				}
			}
		} else{
			// makes sure the previous hash of the first block is all 0s
			for (int i = 0; i < 7; i++){
				if(ledgerPtr->b.data[i] != 0)
					return 1;
			}
		}
		
		// verifies block's block hash
		hash = getMerkleRoot(ledgerPtr->b.transactions, ledgerPtr->b.data[17]);
		
		for (int i = 18; i < 25; i++)
		{
			if (ledgerPtr->b.data[i] != hash[i + 7])
				return 1;
		}

		// verifies block's block hash
		hash = getHash(ledgerPtr->b.data, 25);
		for (int i = 25; i < 32; i++)
			if(ledgerPtr->b.data[i] != hash[i])
				return 1;

		ledgerPtr = ledgerPtr->next;
	}

	// if the block chain has failed verification after going through the entire ledger then it has been verified
	return 0;
}

/* 
	initializeblockChain(int num)
	adds a new block chain element to the ledger and initializes the the block in the block chain element
	with the previous hash, allocates room for transactions in the block, and sets the current 
	blocks's block hash before advancing the ledgerPtr to the new blockChain
	PARAMS: num	- number of transactions to be added to the block chain
  	RETURN:	the ledger pointer
  	PRE:	a ledger exists
  	POST:	the block and head hash of the old block is set, the new block's previous hash has been set,
	  		and space has been allocated for the transactions to be added to the new block
*/
struct blockChain * initializeblockChain(int num){
	struct blockChain *ledgerPtr = ledger;
	unsigned int blockNum = 1;

	// sets up the initial block to be populated with transactions
	// if no transactions have been added before then the previous hash in the block's header is set to 0
	if(ledgerPtr->b.transactions == NULL){
		setBlockTimeStamp(ledgerPtr);
		for (int i = 0; i < 7; i++)
			ledgerPtr->b.data[i] = 0x00;
	}
	// if the block chain has had transactions added before
	else {
		// finds the latest block chain element and calculates the new block's block number
		while(ledgerPtr->next != NULL){
			blockNum += 1;
			ledgerPtr = ledgerPtr->next;
		}
		blockNum += 1;
		// allocates memory for the new block in the chain to store the new block in the block chain
		int size = sizeof(struct blockChain);
		struct blockChain * next = malloc(size);
		ledgerPtr->next = next;

		setBlockTimeStamp(ledgerPtr->next);

		// populates the new block's previous hash portion with the current block's head hash value
		for (int i = 0; i < 7; i++)
			ledgerPtr->next->b.data[i] = ledgerPtr->b.data[i + 25];
		
		// assign ledgerPtr to point to the new block chain element holding the new block
		ledgerPtr = ledgerPtr->next;
	}

	// makes sure there the current block chain element's next pointer is pointing to NULL;
	ledgerPtr->next = NULL;

	// puts the new block's block number in the block's header
	*((unsigned int*)(ledgerPtr->b.data + 13)) = blockNum;

	// itializes the new blocks transaction pointer to have enough size for the number of transactions to be assigned to the block
	// with a maximum of 64 transactions per block 
	if (num > MXTXNUM){
		ledgerPtr->b.transactions = malloc(sizeof(transaction) * MXTXNUM);
		ledgerPtr->b.data[17] = MXTXNUM;
	}else{
		ledgerPtr->b.transactions = malloc(sizeof(transaction) * num);
		ledgerPtr->b.data[17] = num;
	}
	return ledgerPtr;
}

/* 
	addblockChain(struct blockChain * ledgerPtr, int num)
	adds a new block chain element to the ledger and initializes the the block in the block chain element
	with the previous hash, allocates room for transactions in the block, and sets the current 
	blocks's block hash before davencing the ledgerPtr to the new blockChain
	PARAMS: ledgerPtr	- a pointer to a block chain element with in a ledger
			num			- number of transactions to be added to the block chain
  	RETURN:	the ledger pointer
  	PRE:	a ledger exists
  	POST:	the block and head hash of the old block is set, the new block's previous hash has been set,
	  		and space has been allocated for the transactions to be added to the new block
*/
struct blockChain * addBlock(struct blockChain * ledgerPtr, int num){
	// calculates the Merkle root of the transactions in the current block
	unsigned char *root = getMerkleRoot(ledgerPtr->b.transactions, ledgerPtr->b.data[17]);
	
	// puts the last 7 bytes of the merkle root into the blockHash portion of the block header
	for (int i = 18; i < 25; i++)
		ledgerPtr->b.data[i] = root[i + 7];
	
	// calculates the headHash of the block of just the fisrt 25 bytes of the header
	unsigned char * headHash = getHash(ledgerPtr->b.data, 25);

	// puts the last 7 bytes of the headHash into the headHash portion of the block header
	for (int i = 25; i < 32; i++)
		ledgerPtr->b.data[i] = headHash[i];

	// creates a new block chain element with a new block
	ledgerPtr->next = malloc(sizeof(struct blockChain));
	// sets the block's time stamp
	setBlockTimeStamp(ledgerPtr->next);
		
	// populates the new block's previous hash portion with the current block's head hash value
	for (int i = 0; i < 7; i++)
		ledgerPtr->next->b.data[i] = ledgerPtr->b.data[i + 25];
	*((unsigned int*)(ledgerPtr->next->b.data + 13)) = *((unsigned int*)(ledgerPtr->b.data + 13)) + 1;
	// advances ledgerPtr to point to the next block chain element
	ledgerPtr = ledgerPtr->next;
	ledgerPtr->next = NULL;

	return ledgerPtr;
}

/* 
	addTransactions(struct transaction * modBuffer)
	adds a new session of transactions at the end of the block chain starting with a new block
	PARAMS: modBuffer	- list of transactions to be added to the block chain
			num			- number of transactions to be added to the block chain
  	RETURN:	none
  	PRE:	a ledger exists and has had space allocated for the first block chain element
  	POST:	a new block is added to a block chain
*/
void addTransactions(transaction * modBuffer, int num){
	struct blockChain * ledgerPtr = ledger;

	// creates and initialize a new block chain element
	ledgerPtr = initializeblockChain(num);

	// this for loop iterates over the modBuffer and adds transactions to blocks, creating a new block for every 64
	for (int i = 0; i < num; i++){
		if(i % MXTXNUM == 0 && i != 0){
			// adds a new block chain element to the ledger
			ledgerPtr = addBlock(ledgerPtr, num - i);

			// itializes the new blocks transaction pointer to have enough size for the number of transactions to be assigned to the block
			// with a maximum of 64 transactions per block 
			if (num - i > MXTXNUM){
				ledgerPtr->b.transactions = malloc(sizeof(transaction) * MXTXNUM);
				ledgerPtr->b.data[17] = MXTXNUM;
			}
			else{
				ledgerPtr->b.transactions = malloc(sizeof(transaction) * num - i);
				ledgerPtr->b.data[17] = num - i;
			}
		}
		// takes a transaction from the mod buffer and puts it in the block's list of transations.
		for (int j = 0; j < 6; j++)
			ledgerPtr->b.transactions[i % MXTXNUM].timeStamp[j] = modBuffer[i].timeStamp[j];
		ledgerPtr->b.transactions[i % MXTXNUM].modification = modBuffer[i].modification;
	}
	// calculates the Merkle root of the transactions in the current block
	unsigned char * root = getMerkleRoot(ledgerPtr->b.transactions, ledgerPtr->b.data[17]);
	
	// puts the last 7 bytes of the merkle root into the blockHash portion of the block header
	for (int j = 18; j < 25; j++)
		ledgerPtr->b.data[j] = root[j + 7];

	// calculates the headHash of the block of just the fisrt 25 bytes of the header
	unsigned char * headHash = getHash(ledgerPtr->b.data, 25);

	// puts the last 7 bytes of the headHash into the headHash portion of the block header
	for (int i = 25; i < 32; i++)
		ledgerPtr->b.data[i] = headHash[i];
}

/* 
	printTimeStamp(unsigned char * timeStamp)
	prints a time stamp stored in a character array
	PARAMS: ledgerPtr	- pointer to the head of a ledger
  	RETURN:	none
  	PRE:	a time stamp has been stored into a character array in the following order:
	  		[years since 2020][month][day of the month(January = 0)][hours][minutes][seconds]
  	POST:	the time stamp has been printed in the following format:
	  		year/month/day hours:minutes:seconds
*/
void printTimeStamp(unsigned char * timeStamp){
	printf("Time Stamp (GM Time):\n");
	printf("%04d/%02d/%02d %02d:%02d:%02d\n", timeStamp[0] + 2020, timeStamp[1] + 1, timeStamp[2], timeStamp[3], timeStamp[4], timeStamp[5]);
}

/* 
	printTransactions()
	prints all transactions contained in the blockchain
	PARAMS: none
  	RETURN:	none
  	PRE:	a ledger has been created and populated
  	POST:	the transactions contained in the block chain pointed to by the ledgerPtr
	  		has been printed
*/
void printTransactions(){

	if(ledger == NULL || ledger->b.transactions == NULL){
		printf("Ledger is empty\n");
		return;
	}

	struct blockChain *ledgerPtr = ledger;
	// loop runs while there are still block chain elements to examine
	while (ledgerPtr != NULL) {
		// for loop runs for every transaction contained in the block as detailed by
		// the block size stored in the block header
		for (int i = 0; i < ledgerPtr->b.data[17]; i++)	{
			printTimeStamp(ledgerPtr->b.transactions[i].timeStamp);
			printf("Modification:\n");
			if(ledgerPtr->b.transactions[i].modification.event_type == 0){ 
				printf("Insert ");
				printf("%c at position ", ledgerPtr->b.transactions[i].modification.character);
				printf("%x\n\n", ledgerPtr->b.transactions[i].modification.position);
			} else
				printf("Delete character at position %x\n\n", ledgerPtr->b.transactions[i].modification.position);
		}
		ledgerPtr = ledgerPtr->next;
	}
}

/* 
	insertCharacter(char * peep, char character, char position)
	inserts a character at the specified position into the specified peep
	PARAMS:	peep		- a pointer to the peep into which the character will be inserted
			character	- the character to be inserted
			position	- the position in the peep where the chararcter will be inserted
  	RETURN:	None
  	PRE:	The peep has been intitailized and isn't already full
  	POST:	The character is inserted in the peep at position
*/
void insertCharacter(char * peep, unsigned char character, unsigned char position){
	// ensures the peep isn't already full
	if (strlen(peep) == PEEPLEN){
		printf("Peep is full\n");
		return;
	// esures the position specified is valid
	} else if (position < 1 || position > strlen(peep) + 1){
		printf("invalid position\n");
		return;
	}
	int length = strlen(peep);
	// shifts characters from the position to the end of the peep right by one spot
	for (int i = length - 1; i >= position - 1; i--)
		peep[i + 1] = peep[i];
	
	// inserts the character at he appropriate spot in the peep
	peep[position - 1] = (unsigned char)character;
	peep[length + 1] = '\0';
}

/* 
	deleteCharacter(char * peep, char position)
	inserts a character at the specified position into the specified peep
	PARAMS:	peep		- a pointer to the peep from which a character will be deleted
			position	- the position in the peep from which to delete a character
  	RETURN:	None
  	PRE:	The peep has been intitailized and isn't already empty
  	POST:	The character at position is deleted and all follwoing characters are moved over to have a continuous peep
*/
void deleteCharacter(char * peep, unsigned char position){
	// ensures the peep isn't already full
	if (strlen(peep) == 0){
		printf("Peep is empty\n");
		return;
	// esures the position specified is valid
	} else if (position < 1 || position > strlen(peep)){
		printf("Invalid position\n");
		return;
	}
	// shift all the characters after the indicated position left one byte, effectively deleting the
	// character at the indicated position
	for (int i = (position - 1); i < strlen(peep) - 1; i++)
		peep[i] = peep[i + 1];

	// set the position that initially held the last character to the null character to indicate the end of
	// peep
	peep[strlen(peep) - 1] = '\0';
}

/* 
	getPeep()
	produces the current peep by replaying all the transactions contained in the block chain
	PARAMS: ledgerPtr	- a pointer to the head of a ledger
  	RETURN:	returns a pointer to a peep that was populated using the transactions contained in the
	  		block chain
  	PRE:	there exists a block chain with transactions
  	POST:	a peep is populated using the transactions in a block chain and the address of the peep is returned
*/
char * getPeep(){
	
	if(ledger == NULL || ledger->b.transactions == NULL){
		printf("Ledger is empty\n");
		return NULL;
	}

	// calls getPeepAtTime with the current time to get all transactions up until the current time, which should be
	// all the transactions currently in the block chain
	struct tm * t;
	time_t timeStamp;

	timeStamp = time(NULL);

	// generates the tm values based on the epoch time just assinged to timeStamp
	t = gmtime(&timeStamp);

	// gets the number of years since 2020 to be able to store the time stamp in 6 bytes
	t->tm_year -= 2020 - 1900;

	return getPeepAtTime(t);
}

/*
	compareTransactionTime(struct Transaction transaction, struct tm * time)
	a helper function to compare the time stamp in a transaction to a specified time
	PARAMS: transaction		- the transaction who's time stamp is being compared
			time			- the time the transaction time stamp is being compared against
  	RETURN:	returns 1 if the transation's time stamp is later than the specified time, other wise returns 0
  	PRE:	the transaction has a proper time stamp and time is set to a specified time
  	POST:	returns 1 or 0 depending on if the specified time is before or after the time stamp of the transaction
*/
int compareTransactionTime(transaction transaction, struct tm * time){
	//char * transactionPtr;

	// assings transactionPtr to point to the transaction as a character pointer
	//transactionPtr = (char *)(&transaction.transaction);

	// compares all the tm elements of the transactions time stamp with the
	// specified time returning 1 if any part of the transactions time stamp
	// is later than the specified time's tm element
	// iterates over the transaction backwards due to little endian storage in 
	// memory
	if (transaction.timeStamp[0] > time->tm_year){
		return 1;
		printf("year");
	} else if(transaction.timeStamp[0] < time->tm_year)
		return 0;

	if (transaction.timeStamp[1] > time->tm_mon){
		return 1;
		printf("month");
	}else if(transaction.timeStamp[1] < time->tm_mon)
		return 0;
	
	if (transaction.timeStamp[2] > time->tm_mday)
	{
		return 1;
		printf("day");
	} else if(transaction.timeStamp[2] < time->tm_mday)
		return 0;
	
	if (transaction.timeStamp[3] > time->tm_hour)
	{
		return 1;
		printf("hour");
	} else if(transaction.timeStamp[3] < time->tm_hour)
		return 0;
	
	if (transaction.timeStamp[4] > time->tm_min)
	{
		return 1;
		printf("min");
	} else if(transaction.timeStamp[4] < time->tm_min)
		return 0;

	if (transaction.timeStamp[5] > time->tm_sec){
		return 1;
		printf("second");
	} else if(transaction.timeStamp[5] < time->tm_sec)
		return 0;

	return 0;
}

/* 
	getPeepAtTime(time_t time)
	produces the peep at a certain time stamp by replaying all the transactions in the block chain up to (and including) the time stamp
	PARAMS: ledgerPtr	- a pointer to the head of a ledger
			time		- the time to which the peep reproduced
  	RETURN: return the peep at the time of time
  	PRE:	a ledger has been populated with transactions
  	POST:	a peep has been reproduced up to a certain time and is returned
*/
char * getPeepAtTime(struct tm *time){
	
	if(ledger == NULL || ledger->b.transactions == NULL){
		printf("Ledger is empty\n");
		return NULL;
	}
	
	struct blockChain *ledgerPtr = ledger;
	char * peep;
	unsigned char character, position;

	// assign 256 characters to peep
	peep = malloc(PEEPLEN);
	peep[0] = '\0';
	// while there are block chain elements left to be examined
	while (ledgerPtr != NULL){
		// for every transaction as indicated by the 17th character in the blocks data portion, which indicates the number of transactions
		// contained in the block
		for (int i = 0; i < ledgerPtr->b.data[17]; i++){
			// if the specified time is before the time stamp of the transaction then the peep up until before this transaction
			if (compareTransactionTime(ledgerPtr->b.transactions[i], time)){
				printf("End %s", peep);
				return peep;
			}
				

			// get the position of the mofication
			position = ledgerPtr->b.transactions[i].modification.position;

			// checks the event_type bit to determine which type of modification to perform (0 -> insert; 1 -> delete)
			if (ledgerPtr->b.transactions[i].modification.event_type == 0){
				// get the character to insert into the peep
				character = ledgerPtr->b.transactions[i].modification.character;
				// insert the character into the peep at the appropriate position
				insertCharacter(peep, character, position);
			} else {
				// delete the character at the indicated position
				deleteCharacter(peep, position);
			}
		}
		ledgerPtr = ledgerPtr->next;
	}
	return peep;
}

/*------------------------------------------------------------------------------------------------------------
# Additional functions
*-----------------------------------------------------------------------------------------------------------*/
/* 
	initializeLedger()
	intializes the head of a ledger
	PARAMS: None
  	RETURN: None
  	PRE:	None
  	POST:	global ledger pointer is initialized
*/
void initializeLedger(){
	// allocate space for the first element in the block chain element list
	ledger = malloc(sizeof(struct blockChain));
	
	if (ledger == NULL){
		printf("Unable to initialize the ledger\n");
		return;
	}
	// set the pointers to NULL for good measure
	ledger->b.transactions = NULL;
	ledger->next = NULL;
}

/* 
	freeBlockChain(struct blockChain * ledgerPtr)
	recursively frees a ledger and it's elements
	PARAMS: ledgerPtr	- the head of a ledger to be freed
  	RETURN: none
  	PRE:	a populated ledger exists
  	POST:	the entire ledger and it's elements are freed
*/
void freeBlockChain(struct blockChain * ledgerPtr){
	// if the ledgerPtr's next pointer is null then it is the last block chain element in the list
	if(ledgerPtr->next == NULL){
		free(ledgerPtr->b.transactions);
		// set the pointers to NULL for good measure
		ledgerPtr->b.transactions = NULL;
		free(ledgerPtr);
	// otherwise call freeLedge with the ledgerPtr's next blockChain
	// the free the ledgerPtr and it's components
	} else{
		freeBlockChain(ledgerPtr->next);
		free(ledgerPtr->b.transactions);
		// set the pointers to NULL for good measure
		ledgerPtr->b.transactions = NULL;
		ledgerPtr->next = NULL;
		free(ledgerPtr);
	}
}

/* 
	freeLedger()
	the public facing function to free the ledger
	PARAMS: none
  	RETURN: none
  	PRE:	a populated ledger exists
  	POST:	the entire ledger and it's elements are freed
*/
void freeLedger(){

	if(ledger == NULL){
		printf("Ledger is empty\n");
		return;
	}

	struct blockChain * ledgerPtr = ledger;
	freeBlockChain(ledgerPtr);
	ledger = NULL;
}

/* 
	printBlockChain(struct blockChain * ledgerPtr)
	prints the contents of a block chain
	PARAMS: ledgerPtr	- the head of a ledger to be printed
  	RETURN: none
  	PRE:	a populated ledger exists
  	POST:	the entire block chain is printed
*/
void printBlockChain(){

	if(ledger == NULL || ledger->b.transactions == NULL){
		printf("Ledger is empty\n");
		return;
	}

	struct blockChain * ledgerPtr = ledger;
	// while there are still block chain elements to be printed
	while(ledgerPtr != NULL){
		for (int i = 0; i < sizeof(ledgerPtr->b.data); i++){
			if(i == 0){
				printf("Previous Hash:\n");
				printf("%02x", ledgerPtr->b.data[i]);
			}else if(i == 7){
				printf("\n");
				printTimeStamp(ledgerPtr->b.data + 7);
				i = 12;
			}
			else if (i == 13) {
				printf("Block Num: ");
	 			printf("%d", (int)ledgerPtr->b.data[i]);
				i = 16;
			}
			else if (i == 17) {
				printf("\nBlock Size: ");
	 			printf("%d", ledgerPtr->b.data[i]);
			}
			else if (i == 18) {
				printf("\nBlock Hash:\n");
	 			printf("%02x", ledgerPtr->b.data[i]);
			}
			else if (i == 25) {
				printf("\nHead Hash:\n");
	 			printf("%02x", ledgerPtr->b.data[i]);
			}
			else
				printf(":%02x", ledgerPtr->b.data[i]);
	 	}
	 	printf("\n\nTransactions:\n");
	 	
		// setup temporary pointer to save the head of the ledger and then
		// ledger is setup to point to only the current block being pointed
		// to by ledgerPtr so that only that block gets printed
		struct blockChain *tempPtr;
		tempPtr = ledger;
		initializeLedger();
		ledger->b.data[17] = ledgerPtr->b.data[17];
		ledger->b.transactions = ledgerPtr->b.transactions;
		ledger->next = NULL;
		printTransactions();
		ledger = tempPtr;

		ledgerPtr = ledgerPtr->next;
	}
}