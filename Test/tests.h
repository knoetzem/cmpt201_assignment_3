#include "../blockChain.h"

/* 
	setTransactionTimeStamp(unsigned char * timeStamp)
	Assigns a time stamp to a transaction
	PARAMS:	timeStamp   - a pointer to the time stamp portion of a transaction 
  	RETURN:	none
  	PRE:	space is available at the pointer to place 6 characters into memory
  	POST:	a time stamp has been generated and assigned to a transaction's time stamp
            portion
*/
void setTransactionTimeStamp(unsigned char *timeStamp);

/* 
	printPeep(char * peep)
	prints the contents of a peep in the following format:
    1:a 2:b 3:c
	PARAMS:	peep    - the string that contains the peep 
  	RETURN:	none
  	PRE:	a peep has been populated
  	POST:	the peep is printed
*/
void printPeep(char *peep);

/* 
	populateModBuffer(transaction *modBuffer, int count, int length)
	populates a modBuffer with a randomly generated transactions
	PARAMS:	modBuffer   - a pointer to a mod buffer that will hold the transactions
            count       - the number of transaction to add to the mod buffer
            length      - the length of the peep as described by the transactions already in the block chain
  	RETURN:	the new length of the peep as described by the transactions already in the block chain
  	PRE:	the mod buffer is empty has been allocated enough memory for all the transactions to be added
  	POST:	the mod buffer has been populated with transactions
*/
int populateModBuffer(transaction *modBuffer, int count, int length);