/*-----------------------------------------------------------------
# Martin Knoetze
# Assignment 3
# Lab Section: AS02(1), Winter 2021
# CMPUT 201, Winter 2021
# Lab Instructor's Name: Antal Buss
#
# Program Purpose:
# This is the source file for a unit test to test the writing and
# reading of a block chain to and from a binary file
*----------------------------------------------------------------*/
#include "tests.h"

/* 
	main()
    this is the main function for testing the writing and reading of a block chain to and from a 
    binary file
	PARAMS:	none
  	RETURN:	0 if the program has exited normally, a none zero value otherwise
  	PRE:	a pointer to the head of a block chain exists
  	POST:	a block chain has been created and populated with trasnactions. The block chain written
            to a binary file and the ledger is freed of its contents. The ledger is then populated
            with the contents of the binary file and the block chain is printed to show its contents 
            to verify it was properly written and read from the file. 
*/
int main(){
    int count = 70;
    int size = 0;
    char *peep;
    // the mod buffer is populated with 70 transactions and these transactions are added to the
    // block chain.
    transaction *modBuffer = malloc(sizeof(transaction) * count);
    
    initializeLedger();
    
    size = populateModBuffer(modBuffer, count, size);

    addTransactions(modBuffer, count);
    
    // print out the block chain and peep to verify the block chain was properly populated
    printBlockChain();
    
    peep = getPeep();
    printPeep(peep);

    // write the block chain to a binary file entitled blockChainWriteRead.data
    writeBlockChain("blockChainWriteRead.data");

    // re-initialize the ledger
    free(modBuffer);
    freeLedger();

    // attempt to print a block chain, but it is empty .
    printBlockChain();

    initializeLedger();

    // read the block chain from a binary file entitled blockChainWriteRead.data
    readBlockChain("blockChainWriteRead.data");

    // print the block chain and peep to confirm the block chain has been properly read from the binary file
    printBlockChain();

    peep = getPeep();
    printPeep(peep);

    freeLedger();

    return 0;
}