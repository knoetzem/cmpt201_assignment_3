#include "tests.h"

/* 
	setTransactionTimeStamp(unsigned char * timeStamp)
	Assigns a time stamp to a transaction
	PARAMS:	timeStamp   - a pointer to the time stamp portion of a transaction 
  	RETURN:	none
  	PRE:	space is available at the pointer to place 6 characters into memory
  	POST:	a time stamp has been generated and assigned to a transaction's time stamp
            portion
*/
void setTransactionTimeStamp(unsigned char * timeStamp){
	struct tm * tm;
	time_t t;
	
	// gets the epoch time since 1900
	t = time(NULL);

	// generates the tm values based on the epoch time just assinged to timeStamp
	tm = gmtime(&t);

	// gets the number of years since 2020 to be able to store the time stamp in 6 bytes
	tm->tm_year -= 2020 - 1900;

	timeStamp[0] = tm->tm_year;
	timeStamp[1] = tm->tm_mon;
	timeStamp[2] = tm->tm_mday;
	timeStamp[3] = tm->tm_hour;
	timeStamp[4] = tm->tm_min;
	timeStamp[5] = tm->tm_sec;
}

/* 
	printPeep(char * peep)
	prints the contents of a peep in the following format:
    1:a 2:b 3:c
	PARAMS:	peep    - the string that contains the peep 
  	RETURN:	none
  	PRE:	a peep has been populated
  	POST:	the peep is printed
*/
void printPeep(char * peep){
    for (int i = 0; i < strlen(peep); i++){
        // for proper alingment of single digit and double digit numbers with triple digit numbers
        if ((i + 1) < 10)
            printf("00");
        else if ((i + 1) < 100)
            printf("0");
        printf("%d:%c", i + 1, peep[i]);
        
        if ((i + 1) % 10 == 0)
            printf("\n");
        else
            printf(" ");
    }
    printf("\n");
}

/* 
	populateModBuffer(transaction *modBuffer, int count, int length)
	populates a modBuffer with a randomly generated transactions
	PARAMS:	modBuffer   - a pointer to a mod buffer that will hold the transactions
            count       - the number of transaction to add to the mod buffer
            length      - the length of the peep as described by the transactions already in the block chain
  	RETURN:	the new length of the peep as described by the transactions already in the block chain
  	PRE:	the mod buffer is empty has been allocated enough memory for all the transactions to be added
  	POST:	the mod buffer has been populated with transactions
*/
int populateModBuffer(transaction *modBuffer, int count, int length){
    time_t t;
    // keeps track of the number of characters that will be in the peep if the
    // transactions are played out to generate a peep
    int size = 0;
    int random;
    
    // generate random number seeded with the current time
    srand((unsigned) time(&t));

    for (int i = 0; i < count; i++){
        random = rand();
        // allows a 10% chance of randomly generating a delete transaction if there are more
        // than four insert transactions in the modBuffer
        if (random % 100 > 10 || length < 4)
            modBuffer[i].modification.event_type = 0;
        else
            modBuffer[i].modification.event_type = 1;
        
        // if the transaction is to an insert transaction
        if(modBuffer[i].modification.event_type == 0){
            // randomly generate a character that has an ascii decimal value of between 32 and 127
            modBuffer[i].modification.character = (rand() % 95) + 32;
            size++;
            // randomly generate a position to insert the character between 0 and the length of the peep as described by previous
            // transactions already in the block chain
            modBuffer[i].modification.position = (rand() % (length + size)) + 1;
        } else {
            size--;
            // randomly generate a position from which to delete a character between 0 and the length of the peep as described by previous
            // transactions already in the block chain
            modBuffer[i].modification.position = (rand() % (length + size)) + 1;
        }
        setTransactionTimeStamp(modBuffer[i].timeStamp);
    }

    // return the length of the peep as described by the transactions already in the block chain
    return size;
}