/*-----------------------------------------------------------------
# Martin Knoetze
# Assignment 3
# Lab Section: AS02(1), Winter 2021
# CMPUT 201, Winter 2021
# Lab Instructor's Name: Antal Buss
#
# Program Purpose:
# This is the source file for a unit test for testing the verification
# of a block chain
*----------------------------------------------------------------*/
#include "tests.h"

/* 
	main()
    this is the main function for testing the verification of a block chain
	PARAMS:	none
  	RETURN:	0 if the program has exited normally, a none zero value otherwise
  	PRE:	a pointer to the head of a block chain exists
  	POST:	a block chain has been created and populated with trasnactions. The block chain is
            verified to show it's integrity immediatly after being populated. A small edit is
            made to the transactions in the block chain that will cause the block chain to fail
            verification.
*/
int main(){
    int count = 10;
    int size = 0;
    
    transaction *modBuffer = malloc(sizeof(transaction) * count);
    
    initializeLedger();
    
    size = populateModBuffer(modBuffer, count, size);
    addTransactions(modBuffer, count);
    
    // this verification tests the program's ability to test the validity of a block chain with one block
    if(verifyBlockChain() == 0)
        printf("Block chain is verified\n");
    else
        printf("Block chain is not verified\n");
    
    // change the position of a transaction in the block chain
    for (int i = 0; i < ledger->b.data[17]; i++){
        char position = ledger->b.transactions[i].modification.position;
        // if a transaction's position is not 1 then the position is changed to 1
        if(position != 1){
            ledger->b.transactions[i].modification.position = 1;
            break;
        }
        // if all the positions in the transactions are 1 then the position of the first transaction in the 
        // block chain gets changed to position 2
        ledger->b.transactions[0].modification.position = 2;
    }
    // this verification tests the program's ability to test the validity of a block chain with one block
    if (verifyBlockChain() == 0)
        printf("Block chain is verified\n");
    else
        printf("Block chain is not verified\n");

    // re-initialize the ledger and populate the block chain with new transactions
    freeLedger();
    free(modBuffer);
    initializeLedger();

    modBuffer = malloc(sizeof(transaction) * count);
    size = populateModBuffer(modBuffer, count, size);
    addTransactions(modBuffer, count);
    free(modBuffer);
    modBuffer = malloc(sizeof(transaction) * count);
    size += populateModBuffer(modBuffer, count, size);
    addTransactions(modBuffer, count);

    // this verification tests the program's ability to test the validity of a block chain with more than one block
    if(verifyBlockChain() == 0)
        printf("Block chain is verified\n");
    else
        printf("Block chain is not verified\n");

    // change the position of a transaction in the block chain
    for (int i = 0; i < ledger->next->b.data[17]; i++){
        char position = ledger->next->b.transactions[i].modification.position;
        // if a transaction's position is not 1 then the position is changed to 1
        if(position != 1){
            ledger->next->b.transactions[i].modification.position = 1;
            break;
        }
        // if all the positions in the transactions are 1 then the position of the first transaction in the 
        // block chain gets changed to position 2
        ledger->next->b.transactions[0].modification.position = 2;
    }

    // this verification tests the program's ability to test the validity of a block chain with more than one block
    if(verifyBlockChain() == 0)
        printf("Block chain is verified\n");
    else
        printf("Block chain is not verified\n");

    // re-initialize the ledger and populate the block chain with new transactions
    freeLedger();
    free(modBuffer);

    initializeLedger();

    modBuffer = malloc(sizeof(transaction) * count);
    size = populateModBuffer(modBuffer, count, size);
    addTransactions(modBuffer, count);
    free(modBuffer);
    modBuffer = malloc(sizeof(transaction) * count);
    size += populateModBuffer(modBuffer, count, size);
    addTransactions(modBuffer, count);

    // this verification tests the program's ability to test the validity of a block chain with more than one block
    if(verifyBlockChain() == 0)
        printf("Block chain is verified\n");
    else
        printf("Block chain is not verified\n");

    // modify a character in the header of the first block of the block chain
    ledger->b.data[11] += 1;

    // this verification tests the program's ability to test the validity of a block chain with more than one block
    if(verifyBlockChain() == 0)
        printf("Block chain is verified\n");
    else
        printf("Block chain is not verified\n");

    return 0;
}