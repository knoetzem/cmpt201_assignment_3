/*-----------------------------------------------------------------
# Martin Knoetze
# Assignment 3
# Lab Section: AS02(1), Winter 2021
# CMPUT 201, Winter 2021
# Lab Instructor's Name: Antal Buss
#
# Program Purpose:
# This is the source file for a unit test to test the adding of
# transactions to a block chain as well as the production of a
# peep by replaying all the transactions 
*----------------------------------------------------------------*/
#include "tests.h"

/* 
	main()
    this is the main function for testing the adding of transactions to a block chain
	PARAMS:	none
  	RETURN:	0 if the program has exited normally, a none zero value otherwise
  	PRE:	a pointer to the head of a block chain exists
  	POST:	a block chain has been created and populated with trasnactions. The block chain is printed
            to show its contents and then all memory is freed and the program exits 
*/
int main(){
    int count = 70;
    int size = 0;
    char *peep;
    // the mod buffer is populated with 70 transactions and these transactions are added to the
    // block chain. This to test the adding for more than 64 transactions, the max number of
    // transactions for one block, to a block chain. This should result in the creation of two
    // blocks, one with 64 transactions, and one with 6 transactions
    transaction *modBuffer = malloc(sizeof(transaction) * count);
    
    initializeLedger();
    
    size = populateModBuffer(modBuffer, count, size);

    addTransactions(modBuffer, count);

    // print transactions and then print the block chain to see that all the transactions
    // and then see the transactions in with their respective blocks
    printf("#---------------------------------Transactions----------------------------------------#\n");
    printTransactions();
    printf("#---------------------------------Block Chain----------------------------------------#\n");
    printBlockChain();

    // free the buffer and populated the mod buffer with 4 more transactions and add the transactions to the
    // block chain. This tests adding transactions to an already created block chain. This should result in
    // the creation of another block with 4 transactions
    free(modBuffer);
    count = 4;
    modBuffer = malloc(sizeof(transaction) * count);
    
    size += populateModBuffer(modBuffer, count, size);

    addTransactions(modBuffer, count);
    
    // print transactions and then print the block chain to see that all the transactions
    // and then see the transactions in with their respective blocks
    printf("#---------------------------------Transactions----------------------------------------#\n");
    printTransactions();
    printf("#---------------------------------Block Chain----------------------------------------#\n");
    printBlockChain();

    // free the buffer and populated the mod buffer with 5 more transactions and add the transactions to the
    // block chain. This again tests adding transactions to an already created block chain. This should result in
    // the creation of another block with 5 transactions. The block chain should now have a total of 4 blocks with
    // a total of 79 transactions
    free(modBuffer);
    count = 5;
    modBuffer = malloc(sizeof(transaction) * count);
    
    size += populateModBuffer(modBuffer, count, size);

    addTransactions(modBuffer, count);

    // print transactions and then print the block chain to see that all the transactions
    // and then see the transactions in with their respective blocks
    printf("#---------------------------------Transactions----------------------------------------#\n");
    printTransactions();
    printf("#---------------------------------Block Chain----------------------------------------#\n");
    printBlockChain();

    // gets and prints the current peep as described by the transactions in the block chain to
    // verify that transactions have been added appropriately
    peep = getPeep();

    printPeep(peep);
    
    // free the mod buffer and entire ledger
    free(modBuffer);
    freeLedger();

    // print transactions and then print the block chain to see that all the transactions
    // and then see the transactions in with their respective blocks
    printf("#---------------------------------Transactions----------------------------------------#\n");
    printTransactions();
    printf("#---------------------------------Block Chain----------------------------------------#\n");
    printBlockChain();

    return 0;
}