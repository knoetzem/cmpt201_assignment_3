/*-----------------------------------------------------------------
# Martin Knoetze
# Assignment 3
# Lab Section: AS02(1), Winter 2021
# CMPUT 201, Winter 2021
# Lab Instructor's Name: Antal Buss
#
# Program Purpose:
# This is the header file for the blockChain module
*----------------------------------------------------------------*/

// The maximum number of transactions allowed in one block.
#ifndef BLOCKCHAIN_H
#define BLOCKCHAIN_H

#define MXTXNUM 64
#define PEEPLEN 256

#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <time.h>
#include <openssl/evp.h>

// DATA STRUCTURES

/*
	Structure to Represent a modification [2 bytes]. (modification)
		position	- Position in the peep that the event occured: Maximum position of 256
		character	- The character that was modified  - ASCII codes 32 to 127
		event_type	- Type of event that has occured.
					0 for Insert
					1 for Delete
		
		mod - unsigned short that will store the informantion of a modification in bit fields

		0 0 0 0  0 0 0 0  0 0 0 0  0 0 0 0
		|   position   |  |  character | ^event type

		the first byte of mod will store the position of the character to be modified

		the first seven bits of the second byte will store the character to be modified. It will 
		present characters from ascii code 32 (0100000) up to 127 (1111111)

		the last bit of mod will be used to store the event_type 
*/
typedef struct {
    unsigned char position;
    unsigned char character : 7;
    unsigned char event_type : 1;
} modification;

/*
	Structure to Represent a Transaction [8 bytes] (transaction) 
		timestamp		- Time that the event occurred (EPOCH time since January 1-st 2020). [6 bytes]
		modification	- The modification part of the transaction.                          [2 bytes]
	
	trans: 00 01 02 03 04 05 06 07 
	     0x00 00 00 00 00 00 00 00
	       |   timeStamp   | |mod|
*/
typedef struct {
	unsigned char timeStamp[6];
	modification modification;
} transaction;

/*
	Structure to Represent a Block  [32 bytes for the  head + up to 8*64 bytes for transactions] (block)
		previousHash	- Hash of the Previous block head. 0 if first          [7 bytes]
		timeStamp		- Time that the hashing of the block occured           [6 bytes]
		blockNum		- Incremental number of the block.                     [4 bytes]
		blockSize		- Size of transaction elements in the block            [1 byte]
		blockHash		- The Merkle tree hash of the transactions             [7 bytes]
									
									
		headHash		- Hash of the head elements above                      [7 bytes]
		
		data: 00 01 02 03 04 05 06 07 08 09 10 11 12 13 14 15 16 17 18 19 20 21 22 23 24 25 26 27 28 29 30 31  
		    0x00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00 00
		      |    prevousHash   | |   timeStamp   | | blockNum| || |    blockHash     | |     headHash     |
								   /\ /\ /\ /\ /\ /\		     /\
								   || || || || || ||			 ||
								   || || || || || seconds		 blockSize
								   || || || || minutes
								   || || || hour
								   || || day of month
								   || month
								   years since 2020
		transactions	- A pointer to the transactions contained in the block (maximum of 64 transactions)

	NOTE: 	a hash of length 7 bytes is obtained by taking the last 7 bytes of the sha256 hash 
			The truncation is to be applied only as the final step. 
			All intermediary hashes are full sha256 hashes
*/
typedef struct {
	unsigned char data[32];
	transaction * transactions;
} block;

/*
	Structure to represent a ledger element consisting of a block and a pointer to the next block [56 bytes]
		b 		- the block at the entry in the ledger									[48 bytes]
		next	- pointer to the next block in the ledger, null if last ledgeEntry		[8 bytes]

	ledger 	- a global variable that acts as the head of a linked list of ledger elements

	blockChain is the only struct to not be assigned without typedef since this doesn't allow for assigning next as a pointer
	to another instance of blockChain
*/
struct blockChain{
	block b;
	struct blockChain * next;
} * ledger;

//FUNCTIONS
/* 
	getPeep(blockChain * ledger)
	produces the current peep by replaying all the transactions contained in the block chain
	PARAMS: None
  	RETURN:	returns a pointer to a peep that was populated using the transactions contained in the
	  		block chain
  	PRE:	there exists a block chain with transactions
  	POST:	a peep is populated using the transactions in a block chain and the address of the peep is returned
*/
char * getPeep();

/* 
	getPeepAtTime(blockChain * ledgerPtr, time_t time)
	produces the peep at a certain time stamp by replaying all the transactions in the block chain up to (and including) the time stamp
	PARAMS: time	- the time to which the peep reproduced
  	RETURN: return the peep at the time of time
  	PRE:	a ledger has been populated with transactions
  	POST:	a peep has been reproduced up to a certain time and is returned
*/
char * getPeepAtTime(struct tm * time);

/* 
	readBlockChain(char * fileName)
	reads a block chain from a file
	PARAMS: fileName 	- the name of the file from which to read a blockChain
  	RETURN: none
  	PRE:	the file specified by fileName exists, and the pointer to a ledger  points to NULL
  	POST:	the block chain has been read from the text file and a blockChain linked list 
	  		has been populated with the data from the text file and a pointer to this list 
			has been returned
*/
void readBlockChain(char * fileName);

/* 
	initializeLedger(void)
	intializes the head of a ledger
	PARAMS: None
  	RETURN: None
  	PRE:	None
  	POST:	global ledger pointer is initialized
*/
void initializeLedger();

/* 
	verifyBlockChain(blockChain * ledger)
	verifies the integrity of the block chain
	PARAMS:
  	RETURN:
  	PRE:
  	POST:
*/
int verifyBlockChain();

/* 
	addTransactions(blockChain * ledger, struct transaction * modBuffer)
	adds a new session of transactions at the end of the block chain starting with a new block
	PARAMS: modBuffer	- list of transactions to be added to the block chain
			num			- number of transactions to be added to the block chain
  	RETURN:	none
  	PRE:	a ledger exists and has had space allocated for the first ledger element
  	POST:	a new block is added to a block chain
*/
void addTransactions(transaction * modBuffer, int num);

/* 
	printTransactions(blockChain * ledger)
	prints all transactions contained in the blockchain
	PARAMS: 
	RETURN:	none
  	PRE:	a ledger has been created and populated
  	POST:	the transactions contained in the block chain pointed to by the ledgerPtr
	  		has been printed
*/
void printTransactions();

/* 
	writeBlockChain(blockChain * ledger, char * fileName)
	writes a block chain to a file
	PARAMS:	fileName 	- a string with the name of the file to which the block chain gets writen
  	RETURN:	none
  	PRE:	a ledger of ledger elements of blocks and pointers has been created and populated
  	POST:	the contents of the ledger has been writen to a binary file
*/
void writeBlockChain(char * fileName);

/* 
	freeLedger(struct blockChain * ledgerPtr)
	recursively frees a ledger and it's elements
	PARAMS: none
  	RETURN: none
  	PRE:	a populated ledger exists
  	POST:	the entire ledger and it's elements are freed
*/
void freeLedger();

/* 
	printBlockChain(struct blockChain * ledgerPtr)
	prints the contents of a block chain
	PARAMS: none
  	RETURN: none
  	PRE:	a populated ledger exists
  	POST:	the entire block chain is printed
*/
void printBlockChain();
#endif