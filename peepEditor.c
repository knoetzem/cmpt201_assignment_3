#include "peepEditor.h"
/*-----------------------------------------------------------------
# Michael Fauht
# Assignment 3
# Lab Section: AS02(1), Winter 2021
# CMPUT 201, Winter 2021
# Lab Instructor's Name: Antal Buss
#
# Program Purpose:
# Main file for peepEditor module 
*----------------------------------------------------------------*/
/* 
    setTransactionTimeStamp(unsigned char * timeStamp)
    Assigns a time stamp to a transaction
    PARAMS:    timeStamp   - a pointer to the time stamp portion of a transaction 
      RETURN:    none
      PRE:    space is available at the pointer to place 6 characters into memory
      POST:    a time stamp has been generated and assigned to a transaction's time stamp
            portion
*/
void setTransactionTimeStamp(unsigned char * timeStamp){
    struct tm * tm;
    time_t t;
    
    // gets the epoch time since 1900
    t = time(NULL);

    // generates the tm values based on the epoch time just assinged to timeStamp
    tm = gmtime(&t);

    // gets the number of years since 2020 to be able to store the time stamp in 6 bytes
    tm->tm_year -= 2020 - 1900;

    timeStamp[0] = tm->tm_year;
    timeStamp[1] = tm->tm_mon;
    timeStamp[2] = tm->tm_mday;
    timeStamp[3] = tm->tm_hour;
    timeStamp[4] = tm->tm_min;
    timeStamp[5] = tm->tm_sec;
}



/* 
    editor(char * peep, transaction * modBuffer )
    edits a peep and populates a modBuffer to be used to populate a block chain 
    PARAMS: peep        - the string of characters to be populated
            modBuffer   - list of transactions that describe the peep
    RETURN: the number of transactions added to the mod buffer
    PRE:    a modBuffer and peep have been initialized and allocated space
    POST:   a peep has been edited and the transactions describing the modifications have been
            added to a buffer of modifications
*/
unsigned int editor(char * peep, transaction * modBuffer ) {

    int choice, i, position, size = 0;
    unsigned int trans = 0;
    char chara;
    transaction *mbuffer = modBuffer;


    // Print off Menu

    printf("PEEP EDITOR MENU\nChoose an Option\n");
    printf("1. View Characters\n2. Insert\n3. Delete\n4. Save and Exit\n");

    // Get User Choice

    scanf("%d", &choice);

    // Accept only 1 - 4

    while((choice > 4) || (choice < 1)) {

        printf("Invalid Input. Choose from 1-4");
        scanf("%d", &choice);
    }

    // View characters
    while (choice != 4) {


        if (choice == 1) {

            printf("Current Peep\n");


            for (i = 0; i < size; i++) {

                printf("%d:%c ", i + 1, peep[i]);

                }

            

        }

        //insert character

        else if (choice == 2) {



            // Show current peep

            printf("Current Peep:\n");

            for (i = 0; i < size; i++) {

                printf("%d:%c ", i + 1, peep[i]);

            }

            // prompt for position to insert

            printf("\nPosition to insert: ");
            scanf("%d", &position);

            // Error check input. Only accept from 1 -> length of peep + 1

            while ((position < 1) || (position > (strlen(peep) + 1))) {
                printf("\nEnter a valid position. ");
                scanf("%d", &position);

            }



            // Prompt for character to insert

            printf("\nCharacter to insert: ");

            scanf(" %c", &chara);

            // shift elements forward

            for (i = (strlen(peep)); i >= position; i--) {
                peep[i] = peep[i - 1];
            }

            // insert at position

            peep[position-1] = chara;
            
            // update modBuffer
               
            (mbuffer + trans)->modification.character = peep[position-1];
            (mbuffer + trans)->modification.position = position;
            (mbuffer + trans)->modification.event_type = 1;
            setTransactionTimeStamp((mbuffer + trans)->timeStamp);
            
        
            
            //update peep size and transaction counter
            
            size++;
            trans++;


        }

        else if (choice == 3) {

            if (size == 0) {

                printf("\nNothing to delete.\n");
            }

            else {


                // Display current peep

                printf("Current Peep\n");

                for (i = 0; i < size; i++) {

                    printf("%d:%c ", i + 1, peep[i]);

                }

                // prompt for position to delete

                printf("\nPosition to delete: ");
                scanf("%d", &position);

                // Error check input. Only accept from 1 -> length of peep

                while ((position < 1) || (position > strlen(peep))) {
                    printf("\nEnter a valid position. ");
                    scanf("%d", &position);

                }

                //shift elements

                for (i = (position-1); i < (strlen(peep)); i++) {
                    peep[i] = peep[i+1];
                }

                // update modBuffer
                   
                (mbuffer + trans)->modification.character = peep[position-1];
                (mbuffer + trans)->modification.position = position;
                (mbuffer + trans)->modification.event_type = 1;
                setTransactionTimeStamp((mbuffer + trans)->timeStamp);
                
                
                // update peep size and transaction counter 
                
                size--;
                trans++;

            }

        }

        // Print off Menu

        printf("\n\nPEEP EDITOR MENU\nChoose an Option\n");
        printf("1. View Characters\n2. Insert\n3. Delete\n4. Save and Exit\n");

        scanf("%d", &choice);
    }
    printf("%d", size);
    return trans;
}