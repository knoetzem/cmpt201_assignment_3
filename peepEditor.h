/*-----------------------------------------------------------------
# Michael Fauht
# Assignment 3
# Lab Section: AS02(1), Winter 2021
# CMPUT 201, Winter 2021
# Lab Instructor's Name: Antal Buss
#
# Program Purpose:
# Header file for peepEditor
*----------------------------------------------------------------*/
#ifndef PEEPEDITOR_H
#define PEEPEDITOR_H


#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include "blockChain.h"
/* edits the peep, saves transactions in modBuffer, and returns the number of transactions written in the buffer*/
unsigned int editor(char * peep, transaction * modBuffer );

/* updates timeStamp within transaction struct */
void setTransactionTimeStamp(unsigned char * timeStamp);


#endif 
