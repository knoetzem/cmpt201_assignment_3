CC= gcc
CFLAGS= -Wall -g -std=c99

#blockChain.o: blockChain.c blockChain.h
#	$(CC) $(CFLAGS) $? -c blockChain.c

#tests.o: Test/tests.c Test/tests.h blockChain.c blockChain.h
#	$(CC) $(CFLAGS) $? -c Test/tests.c blockChain.c

blockChainPopulateTest: Test/blockChainPopulateTest.c Test/tests.c Test/tests.h blockChain.c blockChain.h
	$(CC) $(CFLAGS) $? -o $@ -lcrypto

blockChainWriteReadTest: Test/blockChainWriteReadTest.c Test/tests.c Test/tests.h blockChain.c blockChain.h
	$(CC) $(CFLAGS) $? -o $@ -lcrypto

blockChainVerifyTest: Test/blockChainVerifyTest.c Test/tests.c Test/tests.h blockChain.c blockChain.h
	$(CC) $(CFLAGS) $? -o $@ -lcrypto

blockChainAllTests: blockChainPopulateTest blockChainWriteReadTest blockChainVerifyTest

testPopulate : blockChainPopulateTest
	./blockChainPopulateTest > Test/populate.txt

testWriteRead : blockChainWriteReadTest
	./blockChainWriteReadTest > Test/writeRead.txt
	xxd blockChainWriteRead.data > Test/blockChainWriteRead.txt
	rm -f blockChainWriteRead.data

testVerify : blockChainVerifyTest
	./blockChainVerifyTest > Test/verify.txt

testAll: testPopulate testWriteRead testVerify